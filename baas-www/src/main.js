import Vue from "vue";
import iView from "iview";
import VueRouter from "vue-router";
import HomeRouter from "./pages/home/router.js";
import Util from "./util";
import App from "./app.vue";
import "iview/dist/styles/iview.css";
import VueLocalStorage from "vue-localstorage";
import VueBus from "vue-bus";
import AMap from "vue-amap";
import axios from "axios";
import VueLazyload from "vue-lazyload";
import VueCodeMirror from "vue-codemirror";
import "codemirror/lib/codemirror.css";
import VueClipboards from "vue-clipboards";
import moment from "vue-moment";

Vue.use(VueRouter);
Vue.use(iView);
Vue.use(function(Vue) {
  for (let key in Util) {
    Vue.prototype[key] = Util[key];
  }
});
Vue.use(VueLocalStorage);
Vue.use(VueBus);
Vue.use(AMap);
Vue.use(VueLazyload);
Vue.use(VueCodeMirror);
Vue.use(VueClipboards);
Vue.use(moment);
// 初始化vue-amap
AMap.initAMapApiLoader({
  // 申请的高德key
  key: "d32b64be0e422afca8dbab9abe4467be",
  // 插件集合
  plugin: [
    "AMap.Autocomplete",
    "AMap.PlaceSearch",
    "AMap.Scale",
    "AMap.OverView",
    "AMap.ToolBar",
    "AMap.MapType",
    "AMap.PolyEditor",
    "AMap.CircleEditor",
    "AMap.Geolocation",
    "AMap.Geocoder"
  ]
});
// 路由配置
const RouterConfig = {
  mode: "history",
  routes: [].concat(HomeRouter)
};
const router = new VueRouter(RouterConfig);

router.beforeEach((to, from, next) => {
  iView.LoadingBar.start();

  // if (!axios.defaults.headers.common['Authorization'] || !axios.defaults.headers.common['WxaToken']) {
  // Util.storage.then(storage => {
  //   storage.get("Authorization", "WxaToken", "Copyright").then(res => {
  //     if (res[0]) {
  //       axios.defaults.headers.common["Authorization"] = res[0];
  //     }
  //     if (res[1]) {
  //       axios.defaults.headers.common["WxaToken"] = res[1];
  //     }
  //     Vue.prototype["$Copyright"] = res[2];
  //     next();
  //   });
  // });
  axios.defaults.headers.common["Token"] = localStorage.Token;
  axios.defaults.headers.common["BaasToken"] = localStorage.BaasToken;
  next();
  // } else {
  //     next();
  // }
});

router.afterEach((to, from, next) => {
  iView.LoadingBar.finish();
  window.scrollTo(0, 0);
});

new Vue({
  el: "#app",
  router: router,
  render: h => h(App)
});
