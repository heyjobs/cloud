module.exports = () => {
  return async (ctx, next) => {
    ctx.appid = ctx.getKey("x-qingful-appid");
    ctx.appkey = ctx.getKey("x-qingful-appkey");
    ctx.dev = ctx.getKey("x-qingful-dev");
    if (!ctx.appid || !ctx.appkey) {
      throw new Error("Unauthorized");
    }

    // 获取当前的baas
    ctx.baas = await BaaS.Models.baas
      .query({
        where: { appid: ctx.appid, appkey: ctx.appkey, status: 1 }
      })
      .fetch({ withRedisKey: ctx.getAppKey("baas") });
    if (!ctx.baas) {
      throw new Error("BaaS Unauthorized");
    }

    // 获取当前的数据表
    ctx.tables = await BaaS.Models.table
      .query({
        where: { baas_id: ctx.baas.id }
      })
      .fetchAll({ withRedisKey: ctx.getAppKey("tables") });

    // 获取当前的数据表关联
    ctx.relations = await BaaS.Models.relation
      .query({
        where: { baas_id: ctx.baas.id }
      })
      .fetchAll({ withRedisKey: ctx.getAppKey("relations") });

    await next();
  };
};
